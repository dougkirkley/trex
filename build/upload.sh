#!/bin/bash

PROJECT_ID="$1"
PRIVATE_TOKEN="$2"
BINARY="$3"

if [ "$2" == "" ]; then
    echo "Missing parameter! Parameters are PROJECT_ID and PRIVATE_TOKEN.";
    exit 1;
fi

URL=$(curl --request POST \
     --header "Private-Token: $PRIVATE_TOKEN" \
     --form "file=@${BINARY}" \
     "https://gitlab.com/api/v4/projects/${PROJECT_ID}/uploads")

URL=$(echo $URL | jq .'url')

echo $URL