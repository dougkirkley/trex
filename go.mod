module gitlab.com/dougkirkley/trex

go 1.14

require (
	github.com/aws/aws-sdk-go v1.34.5
	github.com/ghodss/yaml v1.0.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
	gopkg.in/yaml.v2 v2.2.4
)
