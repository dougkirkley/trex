package controller

import "gitlab.com/dougkirkley/trex/config"

// Controller is a controller
type Controller interface {
	Run() error
	Config() *config.TrexConfig
}